# opalxls2dataquier

__Objectives:__

* convert Opal Export to dataquieR format
* use the transformed DD to apply data quality reporting using dataquieR

__Content:__

* the OPAL XLS export of the SHIP-COVID-C19 study
* the conversion R script