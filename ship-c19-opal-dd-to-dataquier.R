# DATA DICTIONARY PREPARATIONS FOR DQ-REPORTING OF THE SHIP-COVID-19 STUDY -------------------------
# (Source: OPAL xls Export)

# packages -----------------------------------------------------------------------------------------
library(dataquieR)

# data ---------------------------------------------------------------------------------------------
# get variables
shipc19_dd <- openxlsx::read.xlsx(xlsxFile = "SHIP-COVID-19-CRF.xlsx", sheet = "Variables")

# get categories of variables
shipc19_var_cat <- openxlsx::read.xlsx(xlsxFile = "SHIP-COVID-19-CRF.xlsx", sheet = "Categories")

# remove empty rows --------------------------------------------------------------------------------
shipc19_dd <- shipc19_dd[!is.na(shipc19_dd$index), ]

# renaming dd columns to comply with dataquieR -----------------------------------------------------
shipc19_dd <- plyr::rename(shipc19_dd, c("index"     = "VARIABLE_ORDER",
                                         "name"      = "VAR_NAMES",
                                         "label:de"  = "LONG_LABEL",
                                         "valueType" = "DATA_TYPE"))

# renaming data type "date" ------------------------------------------------------------------------
shipc19_dd$DATA_TYPE[shipc19_dd$DATA_TYPE == "date"] <- "datetime"

# parse VALUE_LABELS from categories ---------------------------------------------------------------
VL <- setNames(cbind.data.frame(shipc19_var_cat$variable, 
                               paste(shipc19_var_cat$name, "=", shipc19_var_cat$label)),
                               nm = c("variable", "lev2lab")
)

# collapse all key values to single data elements:
VLc <- tapply(VL$lev2lab, VL$variable, paste, collapse = " | ")

# add to DD
shipc19_dd$VALUE_LABELS <- VLc[shipc19_dd$VAR_NAMES]

# Interprete Malstroem Categories as Study Segments ------------------------------------------------
myfun1 <- function(x) {
  
  xm <- x
  
  for (i in 1:nrow(xm)) {
    if(any(!is.na(xm[i, grep("Mlstr_area", names(xm))]))) {
      xm$KEY_STUDY_SEGMENT[i] <- xm[i, grep("Mlstr_area", names(xm))][!is.na(xm[i, grep("Mlstr_area", 
                                                                                        names(xm))])]  
    }
  }
  
  xm <- xm[, c("VAR_NAMES", "KEY_STUDY_SEGMENT")]
  
  return(xm)
  
}

# past segments to dd
shipc19_dd <- merge(shipc19_dd, myfun1(x = shipc19_dd), by = "VAR_NAMES")

# Reduce DD to dataquier metadata ------------------------------------------------------------------
# use reference columns of DD
dataquieR_md <- readRDS(system.file("extdata", "ship_meta.RDS", package = "dataquieR"))

# Initialize dataquieR metadata columns
shipc19_dd[setdiff(names(dataquieR_md), names(shipc19_dd))] <- NA
shipc19_dd <- shipc19_dd[, names(dataquieR_md)]

# Initialize MISSING_LIST and JUMP_LIST ------------------------------------------------------------
# NOTE: This information has was not included in the OPAL XLS Export
#       THE SHIP Data-Management has provided these codes
shipc19_dd$MISSING_LIST <- rep("99987 | 99993", length(shipc19_dd$MISSING_LIST))
shipc19_dd$JUMP_LIST <- rep("99983 | 99992", length(shipc19_dd$JUMP_LIST))

# export SHIP-C19 DD -------------------------------------------------------------------------------
rm(dataquieR_md, shipc19_var_cat)
saveRDS(shipc19_dd, file = "shipc19_dd_dataquieR.RDS")
